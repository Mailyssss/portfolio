document.addEventListener('DOMContentLoaded', function () {
  let currentSlide = 0;
  const slides = document.querySelectorAll('.slide');
  const totalSlides = slides.length;

  document.querySelector('.nextBtn').addEventListener('click', () => {
    showSlide(currentSlide + 1);
  });

  document.querySelector('.prevBtn').addEventListener('click', () => {
    showSlide(currentSlide - 1);
  });

  function showSlide(index) {
    slides[currentSlide].classList.remove('visible');
    currentSlide = (index + totalSlides) % totalSlides;
    slides[currentSlide].classList.add('visible');

    // Déplacer le carrousel à gauche si on est à la fin
    if (currentSlide === totalSlides - 1) {
      document.querySelector('.carousel').style.transform = `translateX(-${currentSlide * (100 / (totalSlides - 1))}%)`;
      document.querySelector('.nextBtn').disabled = true; // Désactiver le bouton "Next"
    } else {
      document.querySelector('.carousel').style.transform = `translateX(-${currentSlide * (100 / (totalSlides - 1))}%)`;
      document.querySelector('.nextBtn').disabled = false; // Activer le bouton "Next"
    }

    // Déplacer le carrousel à droite si on est au début
    if (currentSlide === 0) {
      document.querySelector('.carousel').style.transform = 'translateX(0)';
    }
  }
});


