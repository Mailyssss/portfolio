function updateProgressBar(percentage) {
    var progressBar = document.querySelector('.progress');
    progressBar.style.width = percentage + '%';
  }
  
  // Exemple : Mise à jour de la barre de progression avec un pourcentage de 50%
  updateProgressBar(60);