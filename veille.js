function sortArticles(ev) {
    const sortType = ev.target.value;
    const articlesList = document.querySelectorAll(".article");
    const dateActuelle = new Date();
    const dateLimite = new Date();
    const articlesArray = Array.from(articlesList);

    if (sortType === "note") {
        // Utilisez la méthode sort() pour trier les articles par la note
        articlesArray.sort((a, b) => {
            const noteA = a.querySelector(".note").dataset.note;
            const noteB = b.querySelector(".note").dataset.note;
            return noteB - noteA; 
        });
    }

    if (dateActuelle < dateLimite) {
        console.log("La date actuelle est antérieure à la date limite.");
    } else  (dateActuelle > dateLimite);
        console.log("La date actuelle est postérieure à la date limite.");
    

    articlesArray.forEach((articleEL, i) => {
        articleEL.style.order = i;
    });
}
